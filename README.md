Making watching youtube slightly more subtle!

Adds a toolbar button to toggle hiding the annoying sticky titlebar from the top of the screen.